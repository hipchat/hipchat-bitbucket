from functools import wraps
import httplib
import logging

from atlassianconnect.oauth2 import Oauth2Client
import jwt
import os
from flask import request, abort


# Find the stack on which we want to store the database connection.
# Starting with Flask 0.9, the _app_ctx_stack is the correct one,
# before that we need to use the _request_ctx_stack.
import redis

try:
    from flask import _app_ctx_stack as stack
except ImportError:
    from flask import _request_ctx_stack as stack
from pymongo import MongoClient

_log = logging.getLogger(__name__)


class Addon(object):
    def __init__(self, app=None, init=True):
        self.app = app
        if app is not None and init:
            self.init_app(app)

    def init_app(self, app):
        # Use the newstyle teardown_appcontext if it's available,
        # otherwise fall back to the request context
        if hasattr(app, 'teardown_appcontext'):
            app.teardown_appcontext(self.teardown)
        else:
            app.teardown_request(self.teardown)

        if app.config.get('DEBUG', False):
            # These two lines enable debugging at httplib level (requests->urllib3->httplib)
            # You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
            # The only thing missing will be the response.body which is not logged.
            httplib.HTTPConnection.debuglevel = 1

            # You must initialize logging, otherwise you'll not see debug output.
            logging.basicConfig()
            logging.getLogger().setLevel(logging.DEBUG)
            requests_log = logging.getLogger("requests.packages.urllib3")
            requests_log.setLevel(logging.DEBUG)
            requests_log.propagate = True
        else:
            logging.basicConfig()
            logging.getLogger().setLevel(logging.WARN)

        app.events = {}

    def connect_mongo_db(self):
        if os.environ.get('MONGOHQ_URL'):
            c = MongoClient(os.environ['MONGOHQ_URL'])
            db = c.get_default_database()
        else:
            c = MongoClient()
            db = c.test

        return db

    def connect_redis(self):
        redis_url = os.getenv('REDISTOGO_URL', 'redis://localhost:6379')
        return redis.from_url(redis_url)

    def teardown(self, _):
        ctx = stack.top
        if hasattr(ctx, 'mongo_db'):
            # ctx.mongo_db.close()
            pass
        if hasattr(ctx, 'redis'):
            # ctx.redis.close()
            pass

    def fire_event(self, name, obj):
        listeners = self.app.events.get(name, [])
        _log.debug("Firing event %s for %s listeners" % (name, len(listeners)))
        for listener in listeners:
            listener(obj)

    def register_event(self, name, func):
        _log.debug("Registering event: " + name)
        self.app.events.setdefault(name, []).append(func)

    def unregister_event(self, name, func):
        del self.app.events.setdefault(name, [])[func]

    def event_listener(self, func):
        self.register_event(func.__name__, func)
        return func

    @property
    def mongo_db(self):
        ctx = stack.top
        if ctx is not None:
            if not hasattr(ctx, 'mongo_db'):
                ctx.mongo_db = self.connect_mongo_db()
            return ctx.mongo_db

    @property
    def redis(self):
        ctx = stack.top
        if ctx is not None:
            if not hasattr(ctx, 'redis'):
                ctx.redis = self.connect_redis()
            return ctx.redis

    def load_client(self, client_id):
        client_data = self.mongo_db.clients.find_one(Oauth2Client(client_id).id_query)
        if client_data:
            return Oauth2Client.from_map(client_data)
        else:
            _log.warn("Cannot find client: %s" % client_id)
            abort(400)

    def validate_jwt(self, request):
        jwt_data = request.args.get('signed_request', None)
        if not jwt_data:
            abort(401)

        oauth_id = jwt.decode(jwt_data, verify=False)['iss']
        client = self.load_client(oauth_id)
        data = jwt.decode(jwt_data, client.secret)
        return client, data['prn']

    def require_jwt(self, func):
        @wraps(func)
        def inner(*args, **kwargs):
            _log.warn("Validating jwt")
            client, user_id = self.validate_jwt(request)
            kwargs.update({
                "client": client,
                "user_id": user_id})
            return func(*args, **kwargs)
        return inner