from collections import defaultdict
import json
from atlassianconnect import installable
from atlassianconnect.addon import Addon
from atlassianconnect.installable import uninstall_client
from atlassianconnect.installable_notifications import notifications
from atlassianconnect.oauth2 import OauthClientInvalidError
from bitbucket.bitbucket import Bitbucket
import logging
import jwt
import os
from flask import Flask, request, abort, \
    render_template, jsonify, redirect, url_for
from requests import Request, Session
import requests


base_url = os.getenv("BASE_URL", "http://192.168.33.1:5000")

app = Flask(__name__)

app.config.update({
    "DEBUG": True if "true" == os.environ.get("DEBUG") else False,
    "PLUGIN_KEY": os.environ.get("PLUGIN_KEY", "atlassian-labs-hipchat-bitbucket"),
    "ADDON_NAME": os.environ.get("ADDON_NAME", "HipChat Bitbucket Connector"),
    "FROM_NAME": os.environ.get("FROM_NAME", "Bitbucket"),
    "BASE_URL": base_url,
    "AIRBREAK_API_KEY": os.environ.get("AIRBRAKE_API_KEY", "")
})

addon = Addon(app)
app.register_blueprint(installable.create('installable', allow_global=False), url_prefix="/installable")
app.register_blueprint(notifications)

_log = logging.getLogger(__name__)

bitbucket_key = os.environ.get('BITBUCKET_KEY')
bitbucket_secret = os.environ.get('BITBUCKET_SECRET')

OAUTH_TOKEN_CACHE = "oauth-token:{token}"


@app.route('/')
def hello():
    return redirect(url_for('capabilities'))


@app.errorhandler(500)
def internal_error(error):
    if not app.config['DEBUG']:
        _log.error("Notifying airbrake of %s" % error)
        # handler = AirbrakeErrorHandler(api_key=app.config['AIRBREAK_API_KEY'],
        #                                env_name="prod",
        #                                request_url=request.url,
        #                                request_path=request.path,
        #                                request_method=request.method,
        #                                request_args=request.args,
        #                                request_headers=request.headers)
        # handler.emit(error, sys.exc_info())
    else:
        _log.error("500 error: %s: " % error)

    return "We screwed up.  The authorities have been notified.  Please try again later.", 500


@app.route('/capabilities')
def capabilities():
    return jsonify({
        "links": {
            "self":         base_url + "/capabilities",
            "homepage":     base_url
        },
        "key": app.config.get("PLUGIN_KEY"),
        "name": app.config.get("ADDON_NAME"),
        "description": "HipChat connect add-on that sends Bitbucket events to a room",
        "vendor": {
            "name": "Atlassian Labs",
            "url": "https://atlassian.com"
        },
        "capabilities": {
            "installable": {
                "allowGlobal": False,
                "allowRoom": True,
                "callbackUrl": base_url + "/installable/"
            },
            "configurable": {
                "url": base_url + "/configure"
            },
            "hipchatApiConsumer": {
                "scopes": [
                    "send_notification"
                ],
                "fromName": app.config.get("FROM_NAME")
            },
            "webhook": [
                {
                    "url": base_url + "/command",
                    "event": "room_message",
                    "pattern": "^/bb .*"
                }
            ],
        }
    })


@app.route("/commit", methods=['POST'])
def on_commit():
    client_id = request.args.get("client_id")
    _log.warn("Commit request from %s" % client_id)
    client = addon.load_client(client_id)
    _log.warn("Client loaded")
    _log.debug("=== bitbucket post hook ===")
    _log.debug("--- headers:\n%r" % request.headers)
    _log.debug("--- form:\n%r" % request.form)
    payload = request.form["payload"]
    msg = format_commit(json.loads(payload))
    if msg:
        _log.warn("Commit msg formatted: %s" % msg)
        #emoticons = get_emoticons(addon.redis, client)
        #msg = format_emoticons(msg, emoticons)
        #_log.warn("With emoticons: %s" % msg)
        resp, status = send_message(client, msg)
        _log.warn("Message sent")
        return resp, status
    else:
        _log.warn("No commits in msg")
        return '', 204


@app.route('/command', methods=['POST'])
def bb_command():
    db = addon.mongo_db
    body = request.json
    client_id = body['oauth_client_id']
    client = addon.load_client(client_id)
    txt = body['item']["message"]["message"][4:]
    token = get_bitbucket_token(db, client.id)
    if not token:
        send_message(client, "Error: Can't find a valid Bitbucket token")
        return '', 204

    if txt.startswith("list branches"):
        bb = get_bitbucket_client(token)
        repo = db.repositories.find_one({"client_id": client.id,
                                             "room_id": client.room_id,
                                             "reponame": txt.split(' ')[-1]})
        if repo:
            bb.username = repo['username']
            _, branches = bb.get_branches(repo['reponame'])
            send_message(client, "Branches for %s: %s" % (repo['reponame'], ", ".join(branches.keys())))
        else:
            send_message(client, "Error: Branch not found")
    else:
        send_message(client, 'Error: Command not found')
    return '', 204


@app.errorhandler(OauthClientInvalidError)
def handle_oauth_client_invalid(error):
    uninstall_client(error.client.id)
    return '', 204

@app.errorhandler(jwt.ExpiredSignature)
def handle_expired_signature(error):
    return render_template("expired_signature.html"), 403


@app.route('/configure')
@addon.require_jwt
def on_configure(client, user_id):
    db = addon.mongo_db
    token = get_bitbucket_token(db, client.id, user_id)

    if token:
        bb = get_bitbucket_client(token)
        success, user = get_current_user(bb)
        if not success:
            _log.debug("cannot access current user, token is probably bad")
            db.oauth_tokens.remove(token.id_query)
            token = None
        else:
            return render_template("configure.html",
                                   signed_request=client.sign_jwt(user_id),
                                   hipchat_url=client.homepage)

    if not token:
        _log.debug("getting a new auth token")
        bb = Bitbucket()
        success, error = bb.authorize(bitbucket_key, bitbucket_secret, base_url + "/oauth_callback")
        if success:
            auth_url = bb.url('AUTHENTICATE', token=bb.access_token)
            cache = addon.redis
            cache.set(OAUTH_TOKEN_CACHE.format(token=bb.access_token), json.dumps(
                {"secret": bb.access_token_secret,
                 "prn": user_id,
                 "oauthID": client.id}
            ))
            return render_template("configure.html",
                                   auth_url=auth_url,
                                   signed_request=client.sign_jwt(user_id),
                                   hipchat_url=client.homepage)
        else:
            _log.debug("error authenticating: %s" % error)
            abort(500)


@app.route('/configure/<path:repo_identifier>', methods=['POST'])
@addon.require_jwt
def add_repo(repo_identifier, client, user_id):

    db = addon.mongo_db
    token = get_bitbucket_token(db, client.id, user_id)

    username, reponame = split_repo_identifier(repo_identifier, token.bitbucket_user_id)

    repo_data = {
        "client_id": client.id,
        "room_id": client.room_id,
        "username": username,
        "reponame": reponame}
    existing_repo = db.repositories.find_one(repo_data)
    if existing_repo:
        return "Repository already exists", 400

    bb = get_bitbucket_client(token)

    bb.username = username
    success, err = bb.service.all(reponame)
    if success:
        repo_data['hipchat_user_id'] = token.hipchat_user_id
        db.repositories.insert(dict(repo_data))

        post_url = bb.url('SET_SERVICE', username=bb.username, repo_slug=reponame)
        hook_request_data = {"URL": base_url + "/commit?client_id=" + client.id, "type": "POST"}
        _log.debug("Sending over hook request data: %s" % json.dumps(hook_request_data))
        r = Request(
            method="POST",
            url=post_url,
            auth=bb.auth,
            params={},
            data=hook_request_data)
        s = Session()
        resp = s.send(r.prepare())
        if resp.status_code == 200:
            if resp.json()['service']['fields'][0]['value'] == "":
                return """
                    Unable to automatically register the POST service in Bitbucket.  Please try to register
                    manually with this URL: %s""" % hook_request_data['url'], 400

            hook_data = {
                "service_id": resp.json()['id'],
                "bitbucket_user_id": username
            }
            hook_data.update(repo_data)
            _log.debug("Registering hook: %s" % json.dumps(hook_data))
            _log.debug("Hook registration response: %s" % resp.text)
            db.hooks.insert(hook_data)
            return '/'.join([username, reponame]), 201
        else:
            db.repositories.remove(repo_data)
            return "Unable to register the webhook in Bitbucket", 400
    else:
        return "The repository doesn't exist or you do not have admin access", 400


@app.route('/configure/<path:repo_identifier>', methods=['DELETE'])
@addon.require_jwt
def remove_repo(repo_identifier, client, user_id):
    db = addon.mongo_db
    token = get_bitbucket_token(db, client.id, user_id)

    username, reponame = split_repo_identifier(repo_identifier, token.bitbucket_user_id)

    remove_filter = {
        "client_id": client.id,
        "room_id": client.room_id,
        "username": username,
        "reponame": reponame
    }
    db.repositories.remove(remove_filter)
    _log.debug("Trying to remove hook matching: %s" % json.dumps(remove_filter))

    hook = db.hooks.find_one(remove_filter)
    if hook:
        remove_hook(client.id, hook)
    else:
        _log.warn("Cannot find existing hook to de-register")

    # just in case duplicates were added
    db.hooks.remove(remove_filter)

    return '', 204


# noinspection PyUnusedLocal
@app.route('/repositories')
@addon.require_jwt
def repositories(client, user_id):
    db = addon.mongo_db

    repositories = db.repositories.find({"client_id": client.id,
                                         "room_id": client.room_id})

    if repositories:
        repo_name_list = ["%s/%s" % (repo['username'], repo['reponame']) for repo in repositories]
        return jsonify(repositories=repo_name_list)
    else:
        return jsonify(repositories=[])


@app.route('/check_oauth')
@addon.require_jwt
def check_oauth(client, user_id):
    db = addon.mongo_db
    return "true" if get_bitbucket_token(db, client.id, user_id) else "false"


@app.route('/oauth_callback')
def on_oauth_callback():
    db = addon.mongo_db
    request_token = request.args.get('oauth_token')
    oauth_verifier = request.args.get("oauth_verifier")
    bb = Bitbucket()
    cache = addon.redis
    token_data = cache.get(OAUTH_TOKEN_CACHE.format(token=request_token))
    if token_data:
        _log.debug("got token data: |||%s||| " % token_data)
        token_data = json.loads(token_data)
        token_secret = token_data['secret']
        success, error = bb.verify(oauth_verifier, bitbucket_key, bitbucket_secret, request_token, token_secret)
        if success:
            _log.debug("success but here is error: %s" % error)
            _log.debug("getting current user with token %s and secret %s" % (bb.access_token, bb.access_token_secret))
            _, user = get_current_user(bb)
            token = BitbucketOauthToken(token_data["oauthID"], token_data['prn'], user['username'],
                                        bb.access_token, bb.access_token_secret)
            _log.debug("inserting new token: %s" % token.id_query)
            db.oauth_tokens.remove(token.id_query)
            db.oauth_tokens.insert(token.to_map())

            return render_template("oauth_callback_success.html")
        else:
            return render_template("oauth_callback_failure.html", cause="Request token not verified: %s" % error)
    else:
        return render_template("oauth_callback_failure.html", cause="Request token expired")


def remove_hook(client_id, hook):
    db = addon.mongo_db
    token = get_bitbucket_token(db, client_id, hook['hipchat_user_id'])
    bb = get_bitbucket_client(token)
    bb.username = hook['bitbucket_user_id']
    del_url = bb.url('SET_SERVICE', username=bb.username, repo_slug=hook['reponame']) + "%s/" % hook['service_id']
    r = Request(
        method="DELETE",
        url=del_url,
        auth=bb.auth,
        params={})
    s = Session()
    resp = s.send(r.prepare())
    if resp.status_code == 200 or resp.status_code == 204:
        _log.debug("Bitbucket service registration removed successfully")
    else:
        _log.error("Unable to remove bitbucket service registration on %s with id %s" % (
            hook['reponame'], hook['service_id']
        ))


@addon.event_listener
def uninstall(event):
    client_id = event['client'].id
    db = addon.mongo_db
    remove_filter = {"client_id": client_id}

    hooks = db.hooks.find(remove_filter)
    for hook in hooks:
        remove_hook(client_id, hook)

    db.hooks.remove(remove_filter)
    db.repositories.remove(remove_filter)


def split_repo_identifier(repo_identifier, default_user):
    parts = repo_identifier.split('/')
    if len(parts) == 1:
        username = default_user
        reponame = parts[0]
    elif len(parts) == 2:
        username = parts[0]
        reponame = parts[1]
    else:
        username = parts[-2]
        reponame = parts[-1]

    return username, reponame


def get_current_user(bb):
    url = bb.URLS['BASE'] % "user"
    response = bb.dispatch('GET', url, auth=bb.auth)
    try:
        return response[0], response[1]['user']
    except TypeError:
        pass
    return response


def get_bitbucket_token(db, client_id, user_id=None):
    if user_id:
        _log.debug("looking for token: %s" % BitbucketOauthToken(client_id, user_id).id_query)
        token_data = db.oauth_tokens.find_one(BitbucketOauthToken(client_id, user_id).id_query)
    else:
        _log.debug("Looking for any user token for client %s" % client_id)
        token_data = db.oauth_tokens.find_one({"client_id": client_id})
    return None if not token_data else BitbucketOauthToken.from_map(token_data)


def get_bitbucket_client(token):
    bb = Bitbucket(token.bitbucket_user_id)
    bb.authorize(bitbucket_key, bitbucket_secret, access_token=token.id, access_token_secret=token.secret)
    return bb


def send_message(client, msg):
    _log.warn("Preparing to send message")
    token = client.get_token(addon.redis)
    _log.warn("Acquired token")

    base_url = client.capabilities_url[0:client.capabilities_url.rfind('/')]
    _log.warn("Base url found in capabilities")
    resp = requests.post("%s/room/%s/notification?auth_token=%s" % (base_url, client.room_id, token),
                         headers={'content-type': 'application/json'},
                         data=json.dumps({"message": msg}), timeout=10)
    _log.warn("Message sent with status: %s" % resp.status_code)
    return '', resp.status_code


def get_emoticons(redis, client):
    cache_key = "emoticons:%s" % client.id
    emoticons = redis.get(cache_key)
    _log.warn("Emoticons from cache: %s" % emoticons)

    if emoticons:
        try:
            emoticons = json.loads(emoticons)
        except:
            _log.warn("Error loading emoticons")
            emoticons = None

    if not emoticons:
        _log.warn("Retrieving emoticons from hipchat")
        token = client.get_token(addon.redis, scopes=["view_group"])
        base_url = client.capabilities_url[0:client.capabilities_url.rfind('/')]
        headers = {"Authorization": "Bearer %s" % token}
        items = consume_all("%s/emoticon" % base_url, headers)
        emoticons = dict([(item['shortcut'], item['url']) for item in items])
        # set and cache for 6 hours
        redis.setex(cache_key, json.dumps(emoticons), 60 * 60 * 6)

    return emoticons


def consume_all(url, headers):
    """
    Requests a paged resource and keeps requesting until there are no more items
    """
    items = []
    resp = requests.get(url, headers=headers, timeout=10)
    try:
        items.extend(resp.json()['items'])

        while resp.json()['links'].get('next', None):
            resp = requests.get(resp.json()['links']['next'], headers=headers, timeout=10)
            items.extend(resp.json()['items'])

    except KeyError:
        _log.error("Bad JSON: %s" % resp.json())

    return items


def format_emoticons(msg, emoticons):
    new_msg = ''
    shortcut = None
    for c in msg:
        if c == '(' and shortcut is None:
            shortcut = ''
        elif c == ')' and shortcut is not None:
            emoticon = emoticons.get(shortcut, None)
            if emoticon:
                new_msg += '<img src="{url}" alt="{shortcut}">'.format(shortcut=shortcut, url=emoticon)
            else:
                new_msg += '(%s)' % shortcut
            shortcut = None
        elif shortcut is not None:
            if c.isalpha():
                shortcut += c
            else:
                new_msg += '(' + shortcut + c
                shortcut = None
        else:
            new_msg += c

    return new_msg


def format_commit(payload):
    # Copyright (c) 2011, Nathan Skerl
    # All rights reserved.
    #
    # Redistribution and use in source and binary forms, with or without
    # modification, are permitted provided that the following conditions are met:
    # Redistributions of source code must retain the above copyright notice, this
    # list of conditions and the following disclaimer. Redistributions in binary
    # form must reproduce the above copyright notice, this list of conditions and
    # the following disclaimer in the documentation and/or other materials
    # provided with the distribution. Neither the name of NATHAN SKERL nor the
    # names of its contributors may be used to endorse or promote products derived
    # from this software without specific prior written permission. THIS SOFTWARE
    # IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
    # EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    # WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    # DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
    # FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    # DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
    # SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
    # CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
    # LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
    # OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
    # DAMAGE.
    _log.debug("Received commit payload: %s" % json.dumps(payload))

    def propagate_branches(branches, commits):
        for branch, nodes in branches.items():
            all_nodes = []
            while nodes:
                node = nodes.pop()
                all_nodes.append(node)
                parents = commits[node]['parents']
                nodes += [parent for parent in parents if parent in commits]
            branches[branch] = all_nodes

    canon_url = payload['canon_url']
    repo = payload['repository']['absolute_url']
    link = canon_url + repo

    # If there are no commits in the payload, it was purely deletion.
    # Don't post anything.
    if not payload['commits']:
        return

    commits = {}
    branches = defaultdict(list)
    authors = set()

    for commit in payload['commits']:
        node_id = commit['node']
        commits[node_id] = commit
        branches[commit['branch']].append(node_id)
        for branch in commit.get('branches', []):
            branches[branch].append(node_id)
        authors.add(commit['author'])

    # Need to figure out the branches for git
    if branches.pop(None, []):
        propagate_branches(branches, commits)

    if len(authors) == 1:
        author_string = list(authors)[0]
    else:
        author_string = "Multiple authors"

    branch_string = "branch"
    if len(branches) != 1:
        branch_string = "branches"

    pmsg = (u'<b>{0}</b> committed to {1} {2} at '
            u'<a href="{3}">{4}</a>'.format(author_string, len(branches),
                                            branch_string, link, repo))

    for branch in branches:
        pmsg += u'<br><b>On branch "{0}"</b>'.format(branch)

        for commit in branches[branch]:
            commit = commits[commit]
            message = commit['message']
            commit['message'] = message if len(message) < 80 else message[:77] + '...'
            # Paul's code for generation changeset-URL
            # (https://bitbucket.org/paul.okopny/pivotaltrackerbroker)
            commit['url'] = ('%s/' % canon_url
                             + payload['repository']['absolute_url'][1:]
                             + 'changeset/' + commit['node'])
            msg = (u'<br> - {message} '
                   u'(<a href="{url}">{node}</a>)'.format(**commit))

            pmsg += msg

    return pmsg


class BitbucketOauthToken:
    def __init__(self, client_id, hipchat_user_id, bitbucket_username=None, token=None, token_secret=None):
        self.client_id = client_id
        self.hipchat_user_id = hipchat_user_id
        self.bitbucket_user_id = bitbucket_username
        self.id = token
        self.secret = token_secret

    @property
    def id_query(self):
        return {"client_id": self.client_id, "hipchat_user_id": self.hipchat_user_id}

    def to_map(self):
        return {
            'id': self.id,
            'client_id': self.client_id,
            'secret': self.secret,
            'hipchat_user_id': self.hipchat_user_id,
            'bitbucket_user_id': self.bitbucket_user_id
        }

    @staticmethod
    def from_map(data):
        return BitbucketOauthToken(
            data['client_id'],
            data['hipchat_user_id'],
            data['bitbucket_user_id'],
            data['id'],
            data['secret']
        )

